package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	pb "bitbucket.org/vkfont/qr-test/proto"
)

//go:generate protoc -I ../proto --go_out=plugins=grpc:../proto ../proto/test.proto

var (
	port = flag.Int("port", 55000, "The server port")
)

type server struct{}

func (s *server) Send(ctx context.Context, in *pb.QRCodeRequest) (*pb.QRCodeResponse, error) {
	log.Printf("got QR message from the client: %v\n", in.Text)
	return &pb.QRCodeResponse{}, nil
}

func (s *server) Subscribe(in *pb.QRCodeResponse, stream pb.QRCode_SubscribeServer) error {
	items := []string{"test1", "test2", "test3"}

	for _, text := range items {
		item := &pb.QRCodeRequest{Text: text}
		if err := stream.Send(item); err != nil {
			return err
		}
	}
	return nil
}

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	pb.RegisterQRCodeServer(grpcServer, &server{})
	reflection.Register(grpcServer)

	log.Printf("start service on port %v\n", *port)
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
