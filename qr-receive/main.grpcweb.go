package main

// import (
// 	"context"
// 	"flag"
// 	"fmt"
// 	"log"
// 	"net/http"

// 	"github.com/improbable-eng/grpc-web/go/grpcweb"
// 	"google.golang.org/grpc"

// 	pb "bitbucket.org/vkfont/qr-test/proto"
// )

// //go:generate protoc -I ../proto --go_out=plugins=grpc:../proto ../proto/test.proto

// var (
// 	port = flag.Int("port", 55000, "The server port")
// )

// type server struct{}

// func (s *server) Send(ctx context.Context, in *pb.QRCodeRequest) (*pb.QRCodeResponse, error) {
// 	log.Printf("got QR message from the client: %v\n", in.Text)
// 	return &pb.QRCodeResponse{}, nil
// }

// func main() {
// 	flag.Parse()
// 	// lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
// 	// if err != nil {
// 	// 	log.Fatalf("failed to listen: %v", err)
// 	// }

// 	grpcServer := grpc.NewServer()
// 	pb.RegisterQRCodeServer(grpcServer, &server{})
// 	// reflection.Register(grpcServer)

// 	wrappedServer := grpcweb.WrapServer(grpcServer)
// 	handler := func(resp http.ResponseWriter, req *http.Request) {
// 		log.Println("incoming request")
// 		wrappedServer.ServeHTTP(resp, req)
// 	}

// 	httpServer := http.Server{
// 		Addr:    fmt.Sprintf(":%d", *port),
// 		Handler: http.HandlerFunc(handler),
// 	}

// 	log.Printf("start service on port %d\n", *port)
// 	if err := httpServer.ListenAndServe(); err != nil {
// 		log.Fatalf("failed starting http server: %v\n", err)
// 	}

// 	// if err := grpcServer.Serve(lis); err != nil {
// 	// 	log.Fatalf("failed to serve: %v", err)
// 	// }
// }
