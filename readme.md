docker run --rm -it \
    -p 80:8080 -p 443:8443 \
    test /grpcwebproxy --backend_addr=192.168.1.48:55000 --run_tls_server=false

protoc --plugin=protoc-gen-ts=./node_modules/.bin/protoc-gen-ts --js_out=import_style=commonjs,binary:generated --ts_out=service=true:generated test.proto
