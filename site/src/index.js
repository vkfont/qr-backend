import { grpc, BrowserHeaders } from "grpc-web-client";
import { QRCode } from "../generated/proto/test_pb_service"
import { QRCodeRequest, QRCodeResponse } from "../generated/proto/test_pb"

const response = new QRCodeResponse()
const request = new QRCodeRequest()
request.setText('some code')

// https://github.com/improbable-eng/grpc-web/blob/master/ts/docs/invoke.md
grpc.invoke(QRCode.subscribe, {
    request: response,
    host: 'http://localhost:80',
    onHeaders: headers => {
        console.log('headers:', headers)
    },
    onMessage: message => {
        console.log('message from the server:', message.toObject())
    },
    onEnd: (code, msg, trailers) => {
        console.log('end:', code, msg, trailers);
    }
})
